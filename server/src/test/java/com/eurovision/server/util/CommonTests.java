package com.eurovision.server.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.*;

import com.eurovision.server.model.City;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CommonTests {

	@Test
	public void biggestSequenceSimpleTest() {

        List<City> cities = new ArrayList<>();
        cities.add(new City(0, "City0"));
        cities.add(new City(1, "City1"));
        cities.add(new City(2, "City2"));

        ArrayList<Integer> response = Common.generateSequences(cities);

        int[] expectedValue = new int[]{0,1,2}; 
 
        assertThat(response.toArray()).isEqualTo(expectedValue);
        assertThat(response.size()).isEqualTo(3);
    }

    @Test
	public void biggestSequenceWithAdjacentSequencesTest() {

        List<City> cities = new ArrayList<>();
        cities.add(new City(0, "City0"));
        cities.add(new City(3, "City3"));
        cities.add(new City(2, "City2"));
        cities.add(new City(1, "City1"));

        ArrayList<Integer> response = Common.generateSequences(cities);

        int[] expectedValue = new int[]{0,1}; 
 
        assertThat(response.toArray()).isEqualTo(expectedValue);
        assertThat(response.size()).isEqualTo(2);
    }

    @Test
	public void biggestSequenceWithWorstCaseTest() {

        List<City> cities = new ArrayList<>();
        cities.add(new City(5, "City5"));
        cities.add(new City(4, "City4"));
        cities.add(new City(3, "City3"));
        cities.add(new City(2, "City2"));
        cities.add(new City(1, "City1"));

        ArrayList<Integer> response = Common.generateSequences(cities);

        int[] expectedValue = new int[]{1}; 
 
        assertThat(response.toArray()).isEqualTo(expectedValue);
        assertThat(response.size()).isEqualTo(1);
    }

    @Test
	public void biggestSequenceEmptyTest() {

        List<City> cities = new ArrayList<>();

        ArrayList<Integer> response = Common.generateSequences(cities);

        int[] expectedValue = new int[]{}; 
 
        assertThat(response.toArray()).isEqualTo(expectedValue);
        assertThat(response.size()).isEqualTo(0);
    }

    @Test
	public void formatResponseTest() {

        ArrayList<Integer> sequence = new ArrayList<>();
        sequence.add(1);
        sequence.add(2);
        sequence.add(3);

        String response = Common.formatResponse(sequence);

        assertThat(response).isEqualTo("(N = 3) : 1 2 3");
    }
}
