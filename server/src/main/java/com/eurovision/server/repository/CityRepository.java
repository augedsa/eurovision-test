package com.eurovision.server.repository;

// import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.eurovision.server.model.City;
 
@Repository
public interface CityRepository extends PagingAndSortingRepository<City, Long> {
}
