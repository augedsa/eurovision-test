package com.eurovision.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.*;

import com.eurovision.server.model.City;
import com.eurovision.server.repository.CityRepository;
import com.eurovision.server.util.Common;
import com.eurovision.server.service.CityService;

 
@Service
public class CityServiceImpl implements CityService{
     
    @Autowired
    CityRepository repository;

    @Override
    public Page<City> getPagedCities(final int page, final int size)
    {
        
        Pageable pagedListCondition = PageRequest.of(page, size);

        final Page<City> cityList = repository.findAll(pagedListCondition);

        return cityList;
         
    }

    @Override
    public Page<City> getPagedAndSortedByNameCities(final int page, final int size)
    {
        
        Pageable pagedListCondition = PageRequest.of(page, size, Sort.by("name").ascending());

        final Page<City> cityList = repository.findAll(pagedListCondition);

        return cityList;
         
    }

    @Override
    public String getBiggestSequence(List<City> list)
    {
        ArrayList<Integer> biggestSequence = Common.generateSequences(list);

        return Common.formatResponse(biggestSequence);
    }
}