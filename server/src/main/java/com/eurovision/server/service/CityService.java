package com.eurovision.server.service;

import java.util.List;
import org.springframework.data.domain.*;

import com.eurovision.server.model.City;

public interface CityService {
    Page<City> getPagedCities(final int page, final int size);
    Page<City> getPagedAndSortedByNameCities(final int page, final int size);
    String getBiggestSequence(List<City> list);
}