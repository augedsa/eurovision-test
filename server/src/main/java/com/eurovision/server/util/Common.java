package com.eurovision.server.util;

import java.util.*;
import java.lang.Math;

import com.eurovision.server.model.City;

public class Common {

    /**
     * Search the lower and hights bound index with binary search
     * 
     * @param cities
     * @param lastIndexPerLength
     * @param lowerBound
     * @param hightestBound
     * @param indexOfCity
     * @return lower bound to be used
     */
    static public int searchSequenceByLowerValue(List<City> cities, ArrayList<Integer> lastIndexPerLength,
            int lowerBound, int hightestBound, int indexOfCity) {
        while (lowerBound <= hightestBound) {

            int middleSearch = (int) Math.ceil((hightestBound + lowerBound) / 2.f);
            if (cities.get(lastIndexPerLength.get(middleSearch)).getId() < cities.get(indexOfCity).getId()) {
                lowerBound = middleSearch + 1;
            } else {
                hightestBound = middleSearch - 1;
            }
        }

        return lowerBound;
    }

    /**
     * Assign the lower position of precedence and update sequences length last
     * smaller index
     * 
     * @param precedenceIndexOfBiggestSequence
     * @param lastIndexPerLength
     * @param lowerBound
     * @param biggestLongitude
     * @param indexOfCity
     * @return biggest longituded updated
     */
    static public int updateSequenceAndBiggestLongitude(ArrayList<Integer> precedenceIndexOfBiggestSequence,
            ArrayList<Integer> lastIndexPerLength, int lowerBound, int biggestLongitude, int indexOfCity) {
        int newLongitude = lowerBound;

        precedenceIndexOfBiggestSequence.add(lastIndexPerLength.get(newLongitude - 1));
        lastIndexPerLength.set(newLongitude, indexOfCity);

        return (newLongitude > biggestLongitude) ? newLongitude : biggestLongitude;
    }

    /**
     * Backward route with biggest longitude from last index array, fill the
     * sequence
     * 
     * @param cities
     * @param precedenceIndexOfBiggestSequence
     * @param lastIndexPerLength
     * @param biggestLongitude
     * @return biggest sequence of numbers
     */
    static public ArrayList<Integer> generateBiggestSequence(List<City> cities,
            ArrayList<Integer> precedenceIndexOfBiggestSequence, ArrayList<Integer> lastIndexPerLength,
            int biggestLongitude) {
        ArrayList<Integer> biggestSequence = new ArrayList<Integer>(Collections.nCopies(biggestLongitude, 0));
        int k = lastIndexPerLength.get(biggestLongitude);

        for (int i = biggestLongitude - 1; i >= 0; i--) {
            biggestSequence.set(i, cities.get(k).getId());
            k = precedenceIndexOfBiggestSequence.get(k);
        }

        return biggestSequence;
    }

    /**
     * Execute algirithm of biggest sequence, O(nlogn) you have two arrays, one to
     * storage the lastest small index and other to reference the precedence index
     * of the sequence
     * 
     * @param cities
     * @return formatted Array of Sequence
     */
    static public ArrayList<Integer> generateSequences(List<City> cities) {

        ArrayList<Integer> precedenceIndexOfBiggestSequence = new ArrayList<Integer>();
        ArrayList<Integer> lastIndexPerLength = new ArrayList<Integer>(Collections.nCopies(cities.size() + 1, 0));

        int biggestLongitude = 0;

        for (int i = 0; i < cities.size(); i++) {

            int lowerBound = Common.searchSequenceByLowerValue(cities, lastIndexPerLength, 1, biggestLongitude, i);

            biggestLongitude = Common.updateSequenceAndBiggestLongitude(precedenceIndexOfBiggestSequence,
                    lastIndexPerLength, lowerBound, biggestLongitude, i);

        }

        return Common.generateBiggestSequence(cities, precedenceIndexOfBiggestSequence, lastIndexPerLength,
                biggestLongitude);

    }

    /**
     * Get the length and array and transforms it to String
     * 
     * @param sequence
     * @return Format response: (N = nx) X1 X2 ... Xnx
     */
    static public String formatResponse(ArrayList<Integer> sequence) {
        String sequeceFormated = (String) Arrays.stream(sequence.toArray()).reduce("",
                (a, b) -> a.toString() + " " + b.toString());
        sequeceFormated = sequeceFormated.trim();

        String template = "(N = %d) : %s";
        return String.format(template, sequence.size(), sequeceFormated);
    }

}
