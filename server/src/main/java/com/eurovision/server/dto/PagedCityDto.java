package com.eurovision.server.dto;

import java.util.List;
import java.io.Serializable;
import org.springframework.data.domain.*;

import com.eurovision.server.model.City;


public class PagedCityDto implements Serializable{

    private List<City> content;
	private int totalPages;
	private long totalElements;
	private boolean last;
	private int size;
	private int number;

    public PagedCityDto(Page<City> pagedCities){
        this.content = pagedCities.getContent();
        this.totalPages = pagedCities.getTotalPages();
        this.totalElements = pagedCities.getTotalElements();
        this.last = pagedCities.isLast();
        this.number = pagedCities.getNumber();
    }

    public List<City> getContent() {
        return content;
    }

    public void setContent(List<City> content) {
        this.content = content;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    /** GET and SET */
    
}
