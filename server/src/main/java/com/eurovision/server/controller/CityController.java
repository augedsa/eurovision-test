package com.eurovision.server.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.data.domain.*;

import com.eurovision.server.model.City;
import com.eurovision.server.service.CityService;
import com.eurovision.server.dto.PagedCityDto;

@Api(value = "City API")
@RestController
@RequestMapping("/api/cities")
public class CityController {
    @Autowired
    CityService cityService;

    /**
     * Get paged content: cities
     * @param page
     * @param size
     * @return Paged cities contents
     */
    @ApiOperation(value= "Get query of cities", notes= "Get paged content: cities with Id and name")
    @CrossOrigin(origins = "*")
    @GetMapping("/queryByPage")
    public PagedCityDto getPagedCities(@RequestParam("page") int page, @RequestParam("size") int size) {
        
        Page<City> pagedCities = cityService.getPagedCities(page, size);

        PagedCityDto pagedCitiesDto = new PagedCityDto(pagedCities);
        
        return pagedCitiesDto;
    }

    /**
     * Get biggest sequence of ids order by name asc
     * @param limit
     * @return Format response: (N = nx) X1 X2 ... Xnx
     */
    @ApiOperation(value= "Biggest Sequence", notes= "Get biggest Sequence Algorithm")
    @GetMapping("/biggetsSequence")
    public ResponseEntity<String> biggestSequence(@RequestParam("limit") int limit) {

        List<City> list = cityService.getPagedAndSortedByNameCities(0, limit).getContent();

        String response = cityService.getBiggestSequence(list);

        return new ResponseEntity<String>(response, new HttpHeaders(), HttpStatus.OK);

    }

}