# Eurovision Test

## Local setup

### Pre-requisites

I'm using `docker-compose` to excecute the application

### Running with local database (docker).

1. From the repository root run `docker-compose up`
2. Execute the client: http://localhost:4200
3. Execute the server: http://localhost:8080
* Swagger UI for server: http://localhost:8080/swagger-ui.html
* Main endpoint: http://localhost:8080/api/cities/queryByPage?page=1&size=5
* Exercise: http://localhost:8080/api/cities/biggetsSequence?limit=30

### Running tests (docker).

1. From the root of repo run `docker-compose -f .\docker-compose-test.yml up`

## Known issues

1. I didn't have time to finish the unit tests on server side, specifically REST tests (database mocking) 
2. Environment variables in application.properties are making some kind of weird error when use ${DB_HOST}, etc. envs.
