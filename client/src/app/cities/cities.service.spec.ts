import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { CitiesService } from './cities.service';

describe('CitiesService', () => {
  let service: CitiesService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CitiesService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('can test HttpClient.get', (done) => {

    const mockCities = {
      content: [
        { id: 1, name: 'City1' },
        { id: 2, name: 'City2' },
        { id: 3, name: 'City3' },
        { id: 4, name: 'City4' },
        { id: 5, name: 'City5' },
      ],
      totalPages: 10,
      totalElements: 10,
      last: 10,
      size: 10,
      number: 10,
    };

    service.getCities(0, 5)
      .subscribe((res: any) => {
        expect(res).toEqual(mockCities);
        done();
      });

    const req = httpTestingController.expectOne('http://localhost:8080/api/cities/queryByPage?page=0&size=5');
    expect(req.request.method).toEqual('GET');
    req.flush(mockCities);
    httpTestingController.verify();
  });
});
