import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080';
  }

  getCities(page: number, size: number) {
    return this.http.get(`${this.baseUrl}/api/cities/queryByPage?page=${page}&size=${size}`);
  }
}
