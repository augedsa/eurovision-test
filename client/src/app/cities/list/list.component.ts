import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { PageEvent } from '@angular/material/paginator';
import { CitiesService } from '../cities.service';
import { City } from '../cities.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  displayedColumns: string[] = ['id', 'name'];
  dataSource: MatTableDataSource<City>;

  // MatPaginator Inputs
  filter: string = '';
  length: number = 0;
  pageSize: number = 5;
  pageSizeOptions: number[] = [5, 10, 15, 20, 25, 30];

  constructor(private citiesService: CitiesService) { }

  ngOnInit(): void {
    this.updateDataSource(0, this.pageSize);
  }

  paginatorEvent(event?: PageEvent) {
    this.updateDataSource(event.pageIndex, event.pageSize);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  updateDataSource(page: number, size: number) {
    this.citiesService.getCities(page, size).subscribe((data: any) => {
      this.dataSource = new MatTableDataSource<City>(data.content);
      this.length = data.totalElements
      this.filter = '';
    });
  }

}
